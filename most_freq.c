#include "most_freq.h"



node* add_to_linkedlist(node* head, char* line_buffer)
{
    // add the word in line_buffer to the linked_list with head
    // Case 1: when head is NULL

    if (head == NULL)
    {
        head = (node*)malloc(sizeof(node));
        head->frequency = 1;

        head->next = NULL;

        head->word = (char*)malloc(sizeof(char) * 256);
        strcpy(head->word, line_buffer);

        //printf("%p %p\n", line_buffer, head->word);

        return head;
    }
    // Case 2: when there is a linked list
    else
    {
        node* current_node;
        current_node = head;

        // check if we need to insert at first:
        //printf("%s %d\n", line_buffer, strcmp(current_node->word, line_buffer));
        if (strcmp(current_node->word, line_buffer) > 0)
        {
            // create a new node
            node* new_node = (node*)malloc(sizeof(node));
            new_node->frequency = 1;

            new_node->word = (char*)malloc(255 * sizeof(char));
            strcpy(new_node->word, line_buffer);

            new_node->next = current_node;

            // insert to linkedlist
            current_node = new_node;
            return current_node;
        }

        // check if the first node is the same as line_buffer
        else if (strcmp(current_node->word, line_buffer) == 0)
        {
            current_node->frequency++;
            return head;
        }

        else
        {
            while (current_node->next != NULL)
            {
                node *temp_node = current_node;
                current_node = current_node->next;

                // check if we need to insert:
                if (strcmp(current_node->word, line_buffer) > 0)
                {
                    // create a new node
                    node* new_node = (node*)malloc(sizeof(node));
                    new_node->frequency = 1;
                    new_node->word = (char*)malloc(255 * sizeof(char));
                    strcpy(new_node->word, line_buffer);

                    // insert to linkedlist
                    new_node->next = current_node;
                    temp_node->next = new_node;
                    return head;
                }

                // check if the first node is the same as line_buffer
                if (strcmp(current_node->word, line_buffer) == 0)
                {
                    current_node->frequency++;
                    return head;
                }
            }
            // insert in the end
            // create a node
            node* new_node = (node*)malloc(sizeof(node));
            new_node->frequency = 1;
            new_node->word = (char*)malloc(255 * sizeof(char));
            strcpy(new_node->word, line_buffer);
            new_node->next = NULL;

            // add the new node to linked list
            current_node->next = new_node;

            return head;
        }
    }
}


/* Reads the content of the file by line and adds to a linked list sorted alphabetically
* returns the linked list
*/
node* readStringList(FILE * infile)
{
    // initialize a NULL linked list
    node* list_head = NULL;
    char* line_buffer = (char*)malloc(256 * sizeof(char));

    while(readLine(infile, line_buffer) == 0)
    {
        // Case: empty line
        if (line_buffer == "\n")
        {
            continue;
        }
        else
        {
            // add the word in line_buffer to linked list
            list_head = add_to_linkedlist(list_head, line_buffer);

            // reset the values in line_buffer
            memset(line_buffer, '\0', sizeof(line_buffer));
        }

    }
    return list_head;
}


/* read a line of the file pointer, returns 0 if success, else, return 1
*/
int readLine(FILE* infile, char* line_buffer)
{
    if (fgets(line_buffer, 256, infile) != NULL)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}



void printStringList(node* head)
{
    node* current = head;
    printf("Word\t\t\tFrequency\n");
    while (current != NULL) {
        printf("%s\t\t\t%d\n\n", current->word, current->frequency);
        current = current->next;
    }
}


node* getMostFrequent(node *head, unsigned int num_to_select)
{
    node* current_head;
    node* temp_current_head;
    node* current_freq_ones_head;
    node* to_delete;

    // create a list of num_to_select nodes that initialize of freq = 0
    node* freq_ones_head = (node*)malloc(sizeof(node));
    freq_ones_head->frequency = 0;

    freq_ones_head->word = (char*)malloc(sizeof(char) * 256);
    freq_ones_head->next = NULL;

    int i = 1;
    while(i < num_to_select)
    {
        // create node
        node* temp = (node*)malloc(sizeof(node));
        temp->frequency = 0;

        temp->word = (char*)malloc(sizeof(char) * 255);
        temp->next = NULL;

        // link it to freq_ones
        node* current = freq_ones_head;
        while (current->next != NULL)
        {
            current = current->next;
        }
        current->next = temp;
        i++;
    }

    // sort accoring to the most frequent

    // have the lowest one in the head for more optimized solution
    current_head = head;
    while (current_head != NULL)
    {
        temp_current_head = current_head;
        current_head = current_head->next;

        current_freq_ones_head = freq_ones_head;

        // no action needed if head does not meet the min
        if (temp_current_head->frequency < current_freq_ones_head->frequency)
        {
            continue;
        }
        else
        {
            // put the node in its rightful place
            node* temp_freq_ones_head;
            while ((current_freq_ones_head != NULL) && (temp_current_head->frequency >= current_freq_ones_head->frequency))
            {
                temp_freq_ones_head = current_freq_ones_head;
                current_freq_ones_head = current_freq_ones_head->next;
            }
            temp_freq_ones_head->next = temp_current_head;

            temp_current_head->next = current_freq_ones_head;

            // remove the first node from freq_ones_head
            to_delete = freq_ones_head;
            freq_ones_head = freq_ones_head->next;
            free(to_delete);
        }
    }
    return freq_ones_head;
}


void freeStringList(node *head)
{
    node* next_pointer;
    while (head->next != NULL)
    {
        next_pointer = head->next;
        free(head);
        head = next_pointer;
    }
    free(head);
}


int main (int argc, char* argv[])
{
  FILE *fp;
  unsigned int num_to_select;
  int show_other_output;


  num_to_select = (unsigned int)4;
  show_other_output = 0;

  fp = fopen("wordlist.txt", "r");
  // Case: file not found
  if(fp == NULL)
  {
      puts("File not found");
  }
  // Case: file is read
  else
  {
      // read the files and add the words to a sorted list (alphabetical)
      node* head = (node*)malloc(sizeof(node));
      head = readStringList(fp);

      // print the list of words sorted alphabetically
      if (show_other_output != 0)
      {
          printStringList(head);
      }

      // get the N most frequent words
      node* most_freq_head = (node*)malloc(sizeof(node));
      most_freq_head = getMostFrequent(head, num_to_select);

      // print the list of num_to_select words
      printStringList(most_freq_head);

      // freeing all memory before exit
      freeStringList(head);
      freeStringList(most_freq_head);
  }
  fclose(fp);

  return (0);
}









