#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    char* word;
    unsigned int frequency;
    struct node *next;
} node;

node *readStringList(FILE *infile);
int readLine(FILE *infile, char *line_buffer);
node *getMostFrequent(node *head, unsigned int num_to_select);
void printStringList(node *head);
void freeStringList(node *head);

node* add_to_linkedlist(node *head, char *line_buffer);
int main(int argc, char* argv[]);
